<?php

/**
 * @file node.inc
 *
 * Main (ctools) plugin file for "node" override type.
 */

/**
 * Define namespace to prevent conflicts with DART_overrides plugins.
 */

$plugin = array(
  'name' => 'dfp_node_override',
  'title' => t('Content Type'),
  'description' => t('Content Type DFP Override'),
  'html tag name' => 'node-override',
  'token suggestions' => 'node',
  'hook_node_view' => '_dfp_node_override_node_view',
  'hook_views_pre_view' => '_dfp_node_override_views_pre_view',
  'hook_init' => '_dfp_node_override_init',
  'hook_dfp_tag_alter' => 'dfp_node_override_apply_override',
  'hook_dfp_global_targeting_alter' => 'dfp_node_override_dfp_global_targeting_alter',
  'weight' => 1
);

/**
 * Implements hook_init().
 */
function _dfp_node_override_init($override, &$alter_tags) {
  // Write down all dfp_node_overrides  type overrides to $alter_tags['dfp_node_override'].
  // Necessary overrides will be used later in hook_node_view.
  // We need to make sure that we on node/% page so hook_node_view will work here.
  $menu_item = menu_get_item();
  if (is_array($menu_item) && $menu_item['path'] == 'node/%') {
    if ($override->plugin_type == 'dfp_node_override') {
      $alter_tags['dfp_node_override'][] = $override;
    }
  }
}

function dfp_node_override_form_submit(&$form, &$form_state, &$dfp_settings) {
  $type = $form_state['values']['dfp_override_type']['plugin_type'];
  $submitted_settings = $form_state['values']['dfp_overrides_settings_' . $type];
  $dfp_settings['node_types'] = $submitted_settings['node_types'];
}

function dfp_node_override_dfp_settings($options) {
  ctools_include('dependent');
  $form = array();
  $form['node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['node_types']['scope'] = array(
    '#type' => 'radios',
    '#title' => t('Node Types'),
    '#options' => array('global' => 'Global Node Override', 'custom' => 'Select Individual Node Types for Override'),
    '#default_value' => !empty($options['node_types']['scope']) ? $options['node_types']['scope'] : "custom"
  );
  $id = 'dfp_overrides-settings-dfp_node_override-node_types';
  $wrapper_id = $id . '-wrapper';
  $form['node_types']['types'] = array(
    '#title' => t('Content Types'),
    '#type' => 'checkboxes',
    '#input' => TRUE,
    '#default_value' => isset($options['node_types']['types']) ? array_keys(array_filter($options['node_types']['types'])) : array(),
    '#options' => node_type_get_names(),
    '#process' => array('ctools_dependent_process', 'form_process_checkboxes'),
    '#dependency' => array('radio:dfp_overrides_settings_dfp_node_override[node_types][scope]' => array('custom')),
    '#prefix' => '<div id="' . $wrapper_id . '">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Implements hook_node_view().
 */
function _dfp_node_override_node_view(&$alter_tags, $node, $view_mode = 'full') {
  // Hook init filled $alter_tags with all dfp_node_override overrides, so we need to unset ones that not match by node type.
  if (isset($alter_tags['dfp_node_override']) && !empty($alter_tags['dfp_node_override'])) {

    foreach ($alter_tags['dfp_node_override'] as $override) {
      // Global scope goes first.
      if ($view_mode == 'full' && $override->dfp_settings['node_types']['scope'] == 'global') {
        foreach ($override->dfp_settings['dfp_tags']['tags'] as $tag_name) {
          if (!isset($alter_tags[$tag_name])) {
            $alter_tags[$tag_name] = $override;
          }
        }
      }
      // Global scope overrides will be overwritten with custom scope ones.
      elseif ($view_mode == 'full' && $override->dfp_settings['node_types']['types'][$node->type]) {
        foreach ($override->dfp_settings['dfp_tags']['tags'] as $tag_name) {
          if (!isset($alter_tags[$tag_name])) {
            $alter_tags[$tag_name] = $override;
          }
        }
      }
    }
  }
}

/**
 * Implements hook_dfp_tag_alter().
 */
function dfp_node_override_apply_override(&$dfp_tag, $alter_tags) {
  $node = menu_get_object();
  if (!empty($node) && is_array($alter_tags)) {
    foreach ($alter_tags as $alter_tag) {
      if (!empty($alter_tag->dfp_settings['node_types'])) {
        $nodes_settings = $alter_tag->dfp_settings['node_types'];
        if (in_array($node->type, array_values($nodes_settings['types']), TRUE)) {
          $dfp_tag->adunit = $alter_tag->dfp_settings['overrides']['unit_pattern']['override'];
        }
      }
    }
  }
}

/**
 * Implements hook_dfp_global_targeting_alter().
 */
function dfp_node_override_dfp_global_targeting_alter(&$targeting, $alter_tags) {
  $node = menu_get_object();
  foreach ($alter_tags as $alter_tag) {
    if (!empty($alter_tag->dfp_settings['node_types'])) {
      $nodes_settings = $alter_tag->dfp_settings['node_types'];
      if (in_array($node->type, array_values($nodes_settings['types']), TRUE)) {
        if (!empty($alter_tag->dfp_settings['overrides']['targeting']['settings'])) {
          // Delete global values.
          $targeting = array();
          foreach ($alter_tag->dfp_settings['overrides']['targeting']['settings'] as $value) {
            if (!empty($value['global'])) {
              $targeting[] = array(
                'target' => $value['target'],
                'value' => $value['value'],
              );
            }
          }
          // Add test mode if we have ?test=on in url.
          if (!empty($_GET['test']) && $_GET['test'] == 'on') {
            $targeting[] = array(
              'target' => 'test',
              'value' => 'on',
            );
          }
        }
      }
    }
  }
}

/**
 * Inside this peace of code we add node object on node related pages.
 * 'node/%/',
 * We need it to invoke default node contexts.
 *
 * Implements hook_views_pre_view().
 */
function _dfp_node_override_views_pre_view(&$alter_tags, $overrides, $node) {
  if (isset($overrides)) {
    $node_replace = array('node' => $node);
    foreach ($overrides as $override) {
      if (!empty($override->dfp_settings['node_types']['types'])) {
        $node_types = array_values($override->dfp_settings['node_types']['types']);
        if ($override->plugin_type == 'dfp_node_override' && in_array($node->type, $node_types, TRUE)) {
          foreach ($override->dfp_settings['overrides']['targeting']['settings'] as &$value) {
            $value['target'] = dfp_overrides_token_replace((string) $value['target'], $node_replace);
            $value['value'] = dfp_overrides_token_replace((string) $value['value'], $node_replace);
          }
          foreach ($override->dfp_settings['dfp_tags']['tags'] as $tag_name) {
            if (!isset($alter_tags[$tag_name]) && $tag_name) {
              $alter_tags[$tag_name] = $override;
            }
          }
        }
      }
    }
  }
}
