<?php

/**
 * @file node.inc
 *
 * Main (ctools) plugin file for "node" override type
 */

/**
 * Define namespace to prevent conflicts with DART_overrides plugins
 */

$plugin = array(
  'name' => 'dfp_path_override',
  'title' => t('Path Overrides'),
  'description' => t('Path based DFP Override'),
  'html tag name' => 'path-override',
  'token suggestions' => '',
  'hook_init' => '_dfp_path_override_init',
  'hook_dfp_tag_alter' => 'dfp_path_override_apply_override',
  'hook_dfp_global_targeting_alter' => 'dfp_path_override_dfp_global_targeting_alter',
  'weight' => 0
);

function dfp_path_override_form_submit(&$form, &$form_state, &$dfp_settings) {
  $type = $form_state['values']['dfp_override_type']['plugin_type'];
  $submitted_settings = $form_state['values']['dfp_overrides_settings_' . $type];
  $dfp_settings['path_types'] = $submitted_settings['path_types'];
}

function dfp_path_override_dfp_settings($options) {
  ctools_include('dependent');
  $form = array();
  $form['path_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Path Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['path_types']['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#default_value' => !empty($options['path_types']['paths']) ? $options['path_types']['paths'] : ""
  );
  $form['path_types']['load_node'] = array(
    '#type' => 'textfield',
    '#title' => 'Token Load Node',
    '#description' => 'Optionally load the node to allow for token based replacements [current-page:url:args], also here can be node id.',
    '#default_value' => !empty($options['path_types']['load_node']) ? $options['path_types']['load_node'] : ""
  );
  $form['path_types']['load_term'] = array(
    '#type' => 'textfield',
    '#title' => 'Token Load Term',
    '#description' => 'Optionally load the term to allow for token based replacements [current-page:url:args], also here can be taxonomy term id.',
    '#default_value' => !empty($options['path_types']['load_term']) ? $options['path_types']['load_term'] : ""
  );
  return $form;
}

/**
 * Implements hook_init().
 */
function _dfp_path_override_init($override, &$alter_tags) {
  $path = drupal_get_path_alias($_GET['q']);
  if (isset($override->dfp_settings['path_types']['paths'])) {
    $pages = $override->dfp_settings['path_types']['paths'];
    if (drupal_match_path($path, $pages)) {
      foreach ($override->dfp_settings['dfp_tags']['tags'] as $tag_name) {
        if (!empty($tag_name)) {
          $alter_tags[$tag_name] = $override;
        }
      }
    }
  }
  $alter_tags['dfp_path_override'][] = $override;
}

/**
 * Implements hook_dfp_tag_alter().
 */
function dfp_path_override_apply_override(&$dfp_tag, $override) {
  // Fix for node/*/* pages.
  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) != '') {
    $node = node_load(arg(1));
    $node = array('node' => $node);
  }
  if (!empty($override->dfp_settings['overrides'])) {
    $settings = $override->dfp_settings['overrides'];
    if (!empty($settings['unit_pattern']['override'])) {
      $dfp_tag->adunit = isset($node) ? dfp_overrides_token_replace((string) $settings['unit_pattern']['override'], $node) : $settings['unit_pattern']['override'];
    }
  }
}

/**
 * Implements hook_dfp_global_targeting_alter().
 */
function dfp_path_override_dfp_global_targeting_alter(&$targeting, $alter_tag) {
  // Fix for node/*/* pages.
  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) != '') {
    $node = node_load(arg(1));
    $node = array('node' => $node);
  }
  if (!empty($alter_tag->dfp_settings['overrides']['targeting']['settings'])) {
    foreach ($alter_tag->dfp_settings['overrides']['targeting']['settings'] as $value) {
      if ($value['global']) {
        foreach ($targeting as $key => $val) {
          if ($val['target'] == $value['target']) {
            unset($targeting[$key]);
          }
        }
        $targeting[] = array(
          'target' => $value['target'],
          'value' => isset($node) ? dfp_overrides_token_replace((string) $value['value'], $node) : $value['value'],
        );
      }
    }
    // Add test mode if we have ?test=on in url.
    if (!empty($_GET['test']) && $_GET['test'] == 'on') {
      $targeting[] = array(
        'target' => 'test',
        'value' => 'on',
      );
    }
    // Remove duplicates from $targeting.
    $targeting = array_map("unserialize", array_unique(array_map("serialize", $targeting)));
  }
}