<?php

/**
 * @file node.inc
 *
 * Main (ctools) plugin file for "node" override type
 */

$plugin = array(
    'name'=> 'dfp_term_override',
    'title' => t('Vocabulary Terms'),
    'description' => t('Term Page DFP Override'),
    'html tag name' => 'term-override',
    'token suggestions' => 'term',
    'hook_init' => '_dfp_term_override_init',
    'hook_dfp_tag_alter' => 'dfp_term_override_apply_override',
    'hook_dfp_global_targeting_alter' => 'dfp_term_override_dfp_global_targeting_alter',
    'weight' => 2
);

function dfp_term_override_form_submit(&$form, &$form_state, &$dfp_settings) {
  $type = $form_state['values']['dfp_override_type']['plugin_type'];
  $submitted_settings = $form_state['values']['dfp_overrides_settings_' . $type];
  $selected_vid = $submitted_settings['vocab_types']['scope'];
  $dfp_settings['vocab_types'] = array();
  $dfp_settings['vocab_types']['scope'] = $selected_vid;
  $dfp_settings['vocab_types']['term_options_' . $selected_vid] = $submitted_settings['vocab_types']['term_options_' . $selected_vid];
}

function dfp_term_override_dfp_settings($options) {
  ctools_include('dependent');
  $form = array();
  $form['vocab_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vocabularies'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $vocab_options = array();
  $vocab_options['global'] = 'Global Taxonomy Override';
  $vocabs = taxonomy_get_vocabularies();
  foreach ($vocabs as $vocab) {
    $vocab_options[$vocab->vid] = $vocab->name;
  }
  $form['vocab_types']['scope'] = array(
    '#type' => 'radios',
    '#title' => t('Vocabulary'),
    '#options' => $vocab_options,
    '#default_value' => !empty($options['vocab_types']['scope']) ? $options['vocab_types']['scope'] : "global"
  );
  foreach ($vocabs as $vid => $vocab) {
    $id = 'dfp_overrides-settings-term_override-vocab_terms-'.$vid;
    $wrapper_id = $id.'-wrapper';
    $form['vocab_types']['term_options_'.$vid] = array(
        '#type' => 'fieldset',
        '#title' => $vocabs[$vid]->name . ' Terms',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#id' => $id,
        '#dependency' => array('radio:dfp_overrides_settings_term_override[vocab_types][scope]' => array($vid)),
        '#prefix' => '<div id="'.$wrapper_id.'">',
        '#suffix' => '</div>',
        '#process' => array('ctools_dependent_process'),
    );
    $terms = taxonomy_get_tree($vid);
    $term_options = array();
    foreach ($terms as $tid => $term) {
      $term_options[$term->tid] = str_repeat('-', $term->depth).$term->name;
    }
    $form['vocab_types']['term_options_'.$vid]['types'] = array(
        '#type' => 'checkboxes',
        '#input' => TRUE,
        '#required' => FALSE,
        '#default_value' => isset($options['vocab_types']['term_options_'.$vid]['types']) ? array_keys(array_filter($options['vocab_types']['term_options_'.$vid]['types'])) : array(),
        '#options' => $term_options,
    );
  }

  return $form;
}

/**
 * Implements hook_init().
 */
function _dfp_term_override_init($override, &$alter_tags) {
  $add_current = FALSE;
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = taxonomy_term_load(arg(2));
    if ($override->dfp_settings['vocab_types']['scope'] == 'global') {
      $add_current = TRUE;
    }
    else {
      if ($override->dfp_settings['vocab_types']['scope'] == $term->vid) {
          if ($override->dfp_settings['vocab_types']['scope'] == $term->vid) {
            $values = array_filter(array_values($override->dfp_settings['vocab_types']['term_options_' . $term->vid]['types']));
            if(in_array($term->tid, $values)) {
              foreach ($override->dfp_settings['dfp_tags']['tags'] as $tag_name) {
                if (!empty($tag_name)) {
                  $alter_tags[$tag_name] = $override;
                }
              }
            }
          }
        }
      }
    }
    if ($add_current || empty($values)) {
      foreach ($override->dfp_settings['dfp_tags']['tags'] as $tag_name) {
        if (!isset($alter_tags[$tag_name])) {
          $alter_tags[$tag_name] = $override;
        }
      }
    }

    if ($override->plugin_type == 'dfp_term_override') {
      $alter_tags['dfp_term_override'][] = $override;
    }
}

/**
 * implements hook_dfp_tag_alter().
 */
function dfp_term_override_apply_override(&$dfp_tag, $alter_tags) {
  $is_term_page = (arg(0) == 'taxonomy') && (arg(1) == 'term') && is_numeric(arg(2));
  if ($is_term_page && is_array($alter_tags)) {
    $term = taxonomy_term_load(arg(2));
    foreach ($alter_tags as $tag) {
      if ($tag->dfp_settings['vocab_types']['scope'] == $term->vid) {
        $value = array_filter(array_values($tag->dfp_settings['vocab_types']['term_options_' . $term->vid]['types']));
        if (in_array($term->tid, $value, TRUE)) {
          $bool = TRUE;
          $dfp_tag->adunit = $tag->dfp_settings['overrides']['unit_pattern']['override'];
        }
        elseif (empty($value)) {
          $data = $tag;
        }
      }
    }
    if (!isset($bool) && !empty($data)) {
      $dfp_tag->adunit = $data->dfp_settings['overrides']['unit_pattern']['override'];
    }
  }
}

/**
 * Implements hook_dfp_global_targeting_alter().
 */
function dfp_term_override_dfp_global_targeting_alter(&$targeting, $alter_tag) {
  $alter_tags = & drupal_static('dfp_overrides_alter_tags', array());
  $is_term_page = (arg(0) == 'taxonomy') && (arg(1) == 'term') && is_numeric(arg(2));
  if (!empty($alter_tags['dfp_term_override']) && $is_term_page) {
    $alter_tags = $alter_tags['dfp_term_override'];
    $term = taxonomy_term_load(arg(2));
    $data = array();
    $bool = FALSE;
    // Delete default global overrides.
    $targeting = array();
    foreach ($alter_tags as $tag) {
      if ($tag->dfp_settings['vocab_types']['scope'] == $term->vid) {
        $value = array_filter(array_values($tag->dfp_settings['vocab_types']['term_options_' . $term->vid]['types']));
        if (in_array($term->tid, $value, TRUE)) {
          $bool = TRUE;
          dfp_term_override_global_targeting_helper($tag, $targeting);
        }
        elseif (empty($value)) {
          $data = array(
            'values' => $value,
            'tag' => $tag,
          );
        }
      }
    }
    if (!$bool && !empty($data)) {
      dfp_term_override_global_targeting_helper($data['tag'], $targeting);
    }
  }
}

function dfp_term_override_global_targeting_helper(&$tag, &$targeting) {
  if (!empty($tag->dfp_settings['overrides']['targeting']['settings'])) {
    foreach ($tag->dfp_settings['overrides']['targeting']['settings'] as $value) {
      if ($value['global']) {
        $targeting[] = array(
          'target' => $value['target'],
          'value' => $value['value'],
        );
      }
    }
    // Add test mode if we have ?test=on in url.
    if (!empty($_GET['test']) && $_GET['test'] == 'on') {
      $targeting[] = array(
        'target' => 'test',
        'value' => 'on',
      );
    }
    // Remove duplicates from $targeting.
    $targeting = array_map("unserialize", array_unique(array_map("serialize", $targeting)));
  }
}
