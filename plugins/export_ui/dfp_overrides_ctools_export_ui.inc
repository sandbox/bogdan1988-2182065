<?php
/**
 * @file dart_overrides_ctools_export_ui.inc
 *
 * Ctools export-ui plugin for dart_override module
 */

$plugin = array(
  'schema' => 'dfp_tag_override',
  'access' => 'administer dfp tags overrides',
  'create access' => 'administer dfp tags overrides',
  'delete access' => 'administer dfp tags overrides',
  'menu' => array(
    'menu prefix' => 'admin/structure/dfp_ads',
    'menu item' => 'overrides',
    'menu title' => 'DFP Tag Overrides',
    'menu description' => 'Advanced override options for DFP tags.',
  ),
  'title' => t('DFP Tag Overrides'),
  'title singular' => t('override'),
  'title plural' => t('overrides'),
  'title singular proper' => t('Override'),
  'title plural proper' => t('Overrides'),
  'handler' => array(
    'class' => 'dfp_overrides_ui',
    'parent' => 'ctools_export_ui',
  ),
  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'dfp_overrides_form',
    'validate' => '_dfp_overrides_key_val_form_validate',
    'submit' => 'dfp_overrides_form_submit'
  ),
);

/**
 * Implements hook_form().
 */
function dfp_overrides_form(&$form, &$form_state) {
  ctools_include('dependent');

  // We will have many fields with the same name, so we need to be able to
  // access the form hierarchically.
  $form['#tree'] = TRUE;
  // See example_add_more_form_add().
  $export = $form_state['item'];
  $count = 0;
  if (!empty($export->dfp_settings['overrides']['targeting']['settings'])) {
    $count = count($export->dfp_settings['overrides']['targeting']['settings']);
  }
  if (empty($form_state['counter']) && $count < 2) {
    $form_state['counter'] = 1;
  }
  elseif (empty($form_state['add_more_ajax'])) {
    $form_state['counter'] = $count;
  }


  $form['description'] = array(
    '#title' => t('description'),
    '#type' => 'textfield',
    '#default_value' => !empty($export->description) ? $export->description : '',
    '#description' => t('Description for this preset.')
  );

  $all_plugins = dfp_overrides_dfp_override_load();
  $options = array();
  foreach ($all_plugins as $plugin) {
    $options[$plugin['name']] = $plugin['description'];
  }

  $form['dfp_override_type'] = array(
    '#type' => 'fieldset',
    '#title' => t('dfp Override Type'),
    '#collapsible' => TRUE,
    '#tree' => TRUE
  );

  $form['dfp_override_type']['plugin_type'] = array(
    '#title' => t('Override Type'),
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => !empty($export->plugin_type) ? $export->plugin_type : 'dfp_node_override',
    '#description' => t('Description for this override.')
  );

  // Output all form settings here but show/hide using.
  // Ctools 'dependent' plugin.

  $usable_tokens = array();
  foreach ($all_plugins as $type => $plugin) {

    // Dfp settings.
    $id = 'dfp_overrides-settings-' . $type;
    $wrapper_id = $id . '-wrapper';
    $form['dfp_overrides_settings_' . $type] = array(
      '#type' => 'fieldset',
      '#title' => t('%description  - settings', array(
        '%description' => $plugin['description']
      )),
      '#input' => TRUE,
      '#process' => array(
        'ctools_dependent_process'
      ),
      '#dependency' => array(
        'radio:dfp_override_type[plugin_type]' => array(
          $type
        )
      ),
      '#id' => $id,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      '#collapsible' => TRUE,
      '#tree' => TRUE
    );

    // Overriden dfp settings.
    $form['dfp_overrides_settings_' . $type] += dfp_overrides_get_override_settings_form($type, $export->dfp_settings);
    $usable_tokens[] = $plugin['token suggestions'];
  }

  $form += _dfp_override_dfp_settings_form($export->dfp_settings, $form_state);

  $form += dfp_override_get_dfp_tags_selector($export->dfp_settings);
  // Display the user documentation of placeholders supported by.
  // this module, as a description on the last pattern.
  $form['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['token_help']['help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => !empty($usable_tokens) ? $usable_tokens : array('node'),
  );
}

/**
 * Form validation for any form that includes fields created by
 *
 * @param $form
 * @param $form_state
 */
function _dfp_overrides_key_val_form_validate($form, &$form_state) {
  // Check that each target|value pair has both a key and val.
  if (empty($form_state['values']['overrides']['targeting']['settings'])) {
    form_set_error('overrides][targeting][settings]', t('Please fill targeting'));
  }
  else {
    foreach ($form_state['values']['overrides']['targeting']['settings'] as $i => $settings) {
      if (is_int($i)) {
        if (empty($settings['target']) && empty($settings['value']) && empty($form_state['add_more_ajax'])) {
          form_set_error('overrides][targeting][settings][' . $i . '][value', t('Please, fill this field.'));
        }
        if (!empty($settings['target']) && empty($settings['value']) && empty($form_state['add_more_ajax'])) {
          form_set_error('overrides][targeting][settings][' . $i . '][value', t('If you enter a target you must also provide a value.'));
        }
        if (!empty($settings['value']) && empty($settings['target']) && empty($form_state['add_more_ajax'])) {
          form_set_error('overrides][targeting][settings][' . $i . '][target', t('If you enter a value you must also provide a target.'));
        }
      }
    }
  }
}

/**
 * Submit handler for ctools plugin form.
 *
 * @param $form
 * @param $form_state
 */
function dfp_overrides_form_submit(&$form, &$form_state) {
  $type = $form_state['values']['dfp_override_type']['plugin_type'];
  $dfp_settings = array();

  $function = $type . '_form_submit';
  if (function_exists($function)) {
    $function($form, $form_state, $dfp_settings);
  }
  // Save Ad unit pattern override.
  $dfp_settings['overrides'] = array();
  $dfp_settings['overrides']['unit_pattern']['override'] = $form_state['values']['overrides']['unit_pattern']['override'];
  // Save targeting settings.
  $dfp_settings['overrides']['targeting']['settings'] = array();
  foreach ($form_state['values']['overrides']['targeting']['settings'] as $index => $settings) {
    if (is_int($index)) {
      $dfp_settings['overrides']['targeting']['settings'][$index] = $settings;
    }
  }
  $dfp_settings['dfp_tags'] = $form_state['values']['dfp_tags'];
  $form_state['item']->name = $form_state['values']['info']['name'];
  $form_state['item']->description = $form_state['values']['description'];
  $form_state['item']->plugin_type = $type;
  $form_state['item']->dfp_settings = $dfp_settings;
}

/**
 * Helper function to add elements into edit form.
 *
 * @param $options
 * @param $form_state
 * @return array
 */
function _dfp_override_dfp_settings_form($options, &$form_state) {
  $form = array();
  $form['overrides'] = array(
    '#type' => 'vertical_tabs',
    '#title' => t('dfp Overrides'),
    '#input' => TRUE,
    '#tree' => TRUE
  );
  // Tag settings overrides.
  $form['overrides']['unit_pattern'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ad Unit Pattern'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $default_add_unit_pattern = variable_get('dfp_default_adunit', '');
  $form['overrides']['unit_pattern']['override'] = array(
    '#type' => 'textfield',
    '#title' => t('Ad Unit Pattern'),
    '#description' => t('You can override the !settings settings: %default', array(
      '!settings' => l(t('Default Ad Unit Pattern'), 'admin/structure/dfp_ads/settings'),
      '%default' => empty($default_add_unit_pattern) ? t('No default value has been set.') : $default_add_unit_pattern,
    )),
    '#required' => FALSE,
    '#maxlength' => 255,
    '#default_value' => !empty($options['overrides']['unit_pattern']['override']) ? $options['overrides']['unit_pattern']['override'] : "",
  );

  // Targeting options.
  $form['overrides']['targeting'] = array(
    '#type' => 'fieldset',
    '#title' => t('Targeting'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'settings',
  );
  $form['overrides']['targeting']['settings'] = array(
    '#type' => 'markup',
    '#tree' => 'FALSE',
    '#prefix' => '<div id="dfp-targeting-wrapper">',
    '#suffix' => '</div>',
    '#theme' => 'dfp_overrides_target_settings',
  );
  // Create multiple targets.
  for ($i = 0; $i < $form_state['counter']; $i++) {
    $form['overrides']['targeting']['settings'][$i] = array(
      '#prefix' => '<div class="target" id="target-' . $i . '">',
      '#suffix' => '</div>',
      '#element_validate' => array('dfp_overrides_targeting_validate'),
      'target' => array(
        '#type' => 'textfield',
        '#title_display' => 'invisible',
        '#title' => 'Target Name',
        '#size' => 10,
        '#default_value' => !empty($options['overrides']['targeting']['settings'][$i]['target']) ? $options['overrides']['targeting']['settings'][$i]['target'] : "",
      ),
      'value' => array(
        '#type' => 'textfield',
        '#title_display' => 'invisible',
        '#title' => 'Target Value',
        '#size' => 10,
        '#default_value' => !empty($options['overrides']['targeting']['settings'][$i]['value']) ? $options['overrides']['targeting']['settings'][$i]['value'] : "",
      ),
      'global' => array(
        '#type' => 'checkbox',
        '#title_display' => 'invisible',
        '#title' => 'Is Global',
        '#default_value' => !empty($options['overrides']['targeting']['settings'][$i]['global']) ? $options['overrides']['targeting']['settings'][$i]['global'] : FALSE,
      ),
    );
    // Add more targets button.
    $form['overrides']['targeting']['dfp_more_targets'] = array(
      '#type' => 'submit',
      '#value' => 'Add another target',
      '#submit' => array('dfp_override_ajax_more_targets_submit'),
      '#limit_validation_errors' => array(),
      '#ajax' => array(
        'callback' => 'dfp_override_more_targets_js',
        'wrapper' => 'dfp-targeting-wrapper',
        'effect' => 'fade',
      ),
    );
    $form['overrides']['targeting']['dfp_less_targets'] = array(
      '#type' => 'submit',
      '#value' => 'Delete target',
      '#limit_validation_errors' => array(),
      '#submit' => array('dfp_override_ajax_less_targets_submit'),
      '#ajax' => array(
        'callback' => 'dfp_override_more_targets_js',
        'wrapper' => 'dfp-targeting-wrapper',
        'effect' => 'fade',
      ),
    );
  }
  // End of targeting options.
  return $form;
}

/**
 * Validation callback.
 *
 * @param $form
 * @param $form_state
 */
function dfp_overrides_targeting_validate(&$form, &$form_state) {

}

function dfp_override_get_dfp_tags_selector($options) {
  $dfp_tags = dfp_tag_load_all();
  $dfp_options = array();
  foreach ($dfp_tags as $machinename => $dfp_tag) {
    $dfp_options[$machinename] = $dfp_tag->machinename;
  }
  $form['dfp_tags'] = array(
    '#type' => 'fieldset',
    '#title' => t('dfp Tags'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#input' => TRUE,
    '#tree' => TRUE
  );
  $form['dfp_tags']['tags'] = array(
    '#type' => 'checkboxes',
    '#description' => t('Please select the tags that this override affects.'),
    '#default_value' => isset($options['dfp_tags']['tags']) ? array_keys(array_filter($options['dfp_tags']['tags'])) : array(),
    '#options' => $dfp_options,
    '#required' => TRUE
  );
  return $form;
}

/**
 * Ajax callback for add more button.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function dfp_override_more_targets_js(&$form, &$form_state) {
  return $form['overrides']['targeting']['settings'];
}

/**
 * Submit callback for ajax add more button.
 *
 * @param $form
 * @param $form_state
 */
function dfp_override_ajax_more_targets_submit(&$form, &$form_state) {
  $form_state['counter']++;
  $form_state['add_more_ajax'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit callback for ajax add delete button.
 *
 * @param $form
 * @param $form_state
 */
function dfp_override_ajax_less_targets_submit(&$form, &$form_state) {
  if ($form_state['counter'] > 1) {
    $form_state['counter']--;
  }
  $form_state['add_more_ajax'] = TRUE;
  $form_state['rebuild'] = TRUE;
}